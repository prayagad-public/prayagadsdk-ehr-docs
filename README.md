# Prayagad Web Sdk User Documentation

Prayagad Web Sdk can be used to integrate Prayagad powered speech recognition and structure extraction from unstructured data for Medication , Investigation and Obsservation

## Getting Started
In order to use this sdk , add the sdk url to the <script> tag in the HTML page.
https://ehr.prayagad.com/sdk/1.1.5/prayagadsdk-ehr-dev.js

### Prerequisites

You would need a system with a mic.
A browser which supports mic and Websockets. This application is well tested on Chrome and Firefox.
All request made are made on port 8443. So ensure that port is not blocked

### Sample Implementation

Sample1 To show how to authenticate , create session and integrate speech
```javascript
// Sample Program
// Iniialize the Sdk
var prSdk = new PrayagadSdk();
// Authenticate the Sdk before start using.
prSdk.authenticate('tester', 'tester', 'b3e904ca-16bb-418b-bd61-de00bc0812c6', { externalPractitionerId: 'EXT_SUNNY' }).then(() => {
    console.log('Authentication successful');
}).catch((error) => {
    console.log('Auth Failed', error);
});
// tester/tester are developer's username and password. These would be given by Prayagad
// b3e904ca-16bb-418b-bd61-de00bc0812c6 is the orgId that would be given by Prayagad
// externalPractitionerId is the external Id that has been provied by the developer when registering the Pracititioner with the Prayagad System.
// Most of the function in Sdk are Promise based  or Callback based.

// Currently the Sdk supports two Session types, Ehr Document Session and Discharge Summary Session
// Session is an instance that is basically used to access all of Prayagad's modules.( SpeechRecognition, MedicationModule, InvestigationModule, ClinicalProfileModule, ObservationModule).
// So we need to create a session to be able to use the functionality

// EHR Session
//  Only afer the authentication is done , the user can create a Session.
prSdk.authenticate('tester', 'tester', 'b3e904ca-16bb-418b-bd61-de00bc0812c6', { externalPractitionerId: 'EXT_SUNNY' }).then(() => {
    console.log('Authentication successful');
    // Encounter is an interaction between a doctor and a patient. So for every patient/doctor meeting , a new encounter needs to be created Sometimes people tend to use appointment Id incase the EHR system doesn't have a concept of encounter
    // Gender and Age can be passed along. this helps to make predictions better.
    prSdk.createSession({ gender: 'female', age: '32', externalPatientId:'referencPatientId'},'external_encounter_id').then((session) => {
    
      // We can get instance of modules like this
       var speechModule = session.getSpeechRecognitionModule();
       var medModule = session.getMedicationModule();
       var obsModule = session.getObservationModule();
       
       // Suppose there are two textbox in an HTML Page by ids U1 and L1.
       // In order to enable speech to these modules, we need to register them with the session.
       speechModule.addSectionDetails('U1', document.getElementById('U1'), ['go to upper box']);
       speechModule.addSectionDetails('L1', document.getElementById('L1'), ['go to lower box']);
       // First parameter is a unique name given to the box.
       // Second parameter is the documentElement reference being passed
       // Third parameter is the keywords on which we can change sections.
       
       speechModule.startVoiceCapture();
       speechModule.stopVoiceCapture();
       // This would start/stop the speech recogntion. These are typically tied the start/stop icon
       
       // We need to register a callback that is triggered once we start/stop voice capture
       // These listeners would be typically used to highlight the current state of speech recongition.
       sr.addStopListener(()=>{
        console.log('Speech Recongition Stopped');
       });
       sr.addStartListener(()=>{
        console.log('Speech Recongition Started');
       });
    });
}).catch((error) => {
    console.log('Auth Failed', error);
});
```
You can find a similar implementation of above at 
http://sangam.dev.prayagad.com/sdkexample/example2.html

Sample2 - To show how to integrate modules
```javascript
// Assuming we have the session object using the above mentioned steps

var medModule = session.getMedicationModule();
var obsModule = session.getObservationModule();

// MedModule
// We need to provide the doucment element from which the module would be scraping the raw notes(unstructured data)
medModule.addNotesElement(document.getElementById("T1"));
// Now MedModule would be watching the data of textbox T1 all the time and whenever there is a change in data , it looks for structured data

// Whenever there is a change in the text or sth has been deleted manually or information of the medication has been changed , a callback is issued with list of all the medication . Entire information keeps coming over and over again.
// The developer simply needs to replace the exisiting list with the new information and show in UI
medModule.addEntityChangeListener(function (entities) {
    console.log('NEW ENTITIES', entities)
});

// Each of these module , keep trying to correct the phrase given by the speech recognition module. Registering a callback will enable us to replace existing data with corrected phrase.
medModule.addPhraseCorrectionListener(function(phrase){
    console.log('Corrected Phrase',phrase);
});

// Incase the sdk in failing for any issue in the backend or the sdk itself, errors will be emitted on this callback
medModule.addErrorListener(function(phrase){
    console.log('Error',error);
});

// To delete an medication from the entity list provided
medModule.deleteEntity(index);
// index being the index of the medication in the list provided in entities.

// To add a medication manually in the entity list.
// We can get the structure of the entity from 
// http://sangam.dev.prayagad.com/sdkdocs/class/src/lib/entityLib/medication/MedicationData.js~MedicationData.html
// Below api is too add a medication with drugId '1234' which needs to be taken for 10 days twice daily
medModule.addManualEntity({ primary : {drugId : '1234', drugName : 'abcd'}, duration : '10 days', dosage : '1-0-1' });


// If incase the results given to callback of entityChangeListnener have partial data incorrect, you can change the value of these entity itself which would again be using medModule
// index , path , value
// To change the duration of first drug in the list
medModule.editEntity(0 ,'duration', '20 days')

// To change the drugName of first drug in the list
medModule.editEntity(0 ,'primary.drugName', 'crocin');

// Med module can handle most of the cases of duplication, error handling etc
```
http://sangam.dev.prayagad.com/sdkexample/example1.html
The way InvestigationModule and ObservationModule works is very similar to how medication Module works. There might be change in the API names or the structure of Investigation or Observation , but the concepts are on the same line

Sample3 - To show how to save restore a session , close session
```
// If a doctor goes back to a previous encounter to change sth there. The state of the session needs to be restored as it was when the doctor finished the encounter.

// Once the doctor closes the document , two things needs to be done
// 1. The externalDocumentId needs to be linked to the documentId created by the sdk.
// This can be done using the below command
session.updateDocumentId('EXT_DOCUMENT_ID');
// Incase you don't have a concept of documentId , you can always pass the same encounterId / appointmentId that was passed before

// 2. Session needs to be closed. This is VERY IMPORTANT, otherwise ,the sdk will be leaking connections, or taking up memory that is not needed.
session.close();

// When the Doctor comes back to the same enconter as before, 
// instead of calling prSdk.createSession() which would create a new session,
// we need to restore the existing session, with the externalEncounterId and externalDocumentId that weere provied
prSdk.restoreSession('externalEncounterId' , 'externalDocumentId').then((session)=>{
    
});
// This will restore the state and give the session object as well

//If the developer wants to save the state of the session on their side ( eg in DB ) , they can always do so by using the API
var saveStr = session.save();
console.log(saveStr);
```

The source code of these examples is available at 
https://gitlab.com/prayagad-public/prayagadsdk-ehr-docs

## Built With
* Javascript ES6

## Versioning

Current Version 
1.1.5 -> https://ehr.prayagad.com/sdk/1.1.5/prayagadsdk-ehr-dev.js

Previous Stable Version
1.1.4 -> https://ehr.prayagad.com/sdk/1.1.4/prayagadsdk-ehr-dev.js

## Authors
Ganesh MR.  ganeshmr@prayagad.com   


## License

This project is licensed to Prayagad Pvt Ltd

