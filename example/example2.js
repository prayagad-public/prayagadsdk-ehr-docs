var sr = null;
var prSdk = new PrayagadSdk();
prSdk.authenticate('tester', 'tester', 'b3e904ca-16bb-418b-bd61-de00bc0812c6', { externalPractitionerId: 'EXT_SUNNY' }).then(() => {
  prSdk.createSession({ gender: 'female', age: '32', externalPatientId: 'mrn-1322' }, 'ExtEncounterId').then((session) => {
    sr = session.getSpeechRecognitionModule();

    var micCurState = 'off';

    function stopListening() {
      micCurState = 'off';
      $("#micImage").attr("src", "./mic_stop.png");
    }

    function startListening() {
      micCurState = 'on';
      $("#micImage").attr("src", "./mic_start.png");
    }

    function handleMicState() {
      if (micCurState === 'off') {
        sr.startVoiceCapture();
      } else {
        sr.stopVoiceCapture();
      }
    };

    function handleText(textObj, sectionwiseTranscript) {
    }

    function handleAction(event) {
      if (event.action === 'STOP_RECORDING') {
        sr.stopVoiceCapture();
      }
    }

    $(document).ready(function () {
      sr.addStopListener(stopListening);
      sr.addStartListener(startListening);

      // These would be required to enable speech setting
      sr.addSectionDetails('cc', document.getElementById('cc'), ['go to chief complaint', 'go to primary']);
      sr.addSectionDetails('med', document.getElementById('med'), ['go to medication', 'go to medicine advice']);
      sr.addSectionDetails('inv', document.getElementById('inv'), ['go to investigation', 'go to procedures']);
      sr.addSectionDetails('dummy', document.getElementById('dummy'), ['go to dummy', 'trash box']);

      sr.addActionDetails('stop recording', 'STOP_RECORDING');

      document.getElementById('cc').addEventListener('focus', () => { sr.changeCurrentActiveSection('cc') });
      document.getElementById('med').addEventListener('focus', () => { sr.changeCurrentActiveSection('med') });
      document.getElementById('inv').addEventListener('focus', () => { sr.changeCurrentActiveSection('inv') });
      document.getElementById('dummy').addEventListener('focus', () => { sr.changeCurrentActiveSection('dummy') });

      sr.addTextListener(handleText);
      sr.addActionListener(handleAction);
      $("#micState").on('click', handleMicState);
    });

  })
});

